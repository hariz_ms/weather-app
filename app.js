const forecast = require("./forecast");
const geocode = require("./geocode");

const address = process.argv[2];

if (address) {
  geocode(address, (error, { location, latitude, longitude }) => {
    if (error) {
      return console.log(error);
    }
    console.log(location);
    forecast(longitude, latitude, (error, data) => {
      if (error) console.log(error);
      else
        console.log(
          `In ${data.name} Temperature is ${data.main.temp}, But It Feels Like ${data.main.feels_like}`
        );
    });
  });
} else {
  console.log("Location Is Not Provided Or It Is Incorrect");
}
